#include <iostream>

// Function prints into console even or odd number
void PrintEvenOrOddNumbers(int LastNumber, bool Even)
{
	for (int i = Even ? 0 : 1; i <= LastNumber; i += 2)
	{
		std::cout << i << " ";
	}
}

int main()
{
	const int LastNumber = 100;

	// with 'if' statement
	for (int i = 0; i <= LastNumber; i++)
	{
		if (i % 2 == 0)
		{
			std::cout << i << " ";
		}
	}

	std::cout << "\n";

	// w/o 'if' statement
	for (int i = 0; i <= LastNumber; i += 2)
	{
		std::cout << i << " ";
	}

	std::cout << "\n";

	PrintEvenOrOddNumbers(LastNumber, false);

	return 0;
}